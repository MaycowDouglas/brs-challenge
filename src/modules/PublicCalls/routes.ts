import { Router } from 'express'
import PublicCallsController from './controller'

const router = Router()
const publicCallsController = new PublicCallsController()

router.get('/', publicCallsController.list)
router.get('/:id', publicCallsController.findUnique)
router.get('/resource/:resource', publicCallsController.list)

export default router
