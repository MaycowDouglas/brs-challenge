import { Request, Response } from 'express'
import axios from 'axios'
import { load } from 'cheerio'

type PublicCallProps = {
  id: number
  attributes: {
    title: string
    schedule: {
      publication: string
      deadline: string
    }
    source: {
      uri: string
    }
  }
  links: {
    self: string
  }
}

type PublicCallDetailsProps = {
  id: number
  attributes: {
    title: string
    description: string
    schedule: {
      publication: string
      deadline: string
    }
    source: {
      uri: string
    }
  }
}

export default class PublicCallsController {
  async list(req: Request, res: Response) {
    const { resource } = req.params

    const proxy = 'https://api.codetabs.com/v1/proxy?quest='
    const baseUrl = `http://www.finep.gov.br/chamadas-publicas`
    const path =
      resource !== undefined
        ? `/chamadaspublicas?frecurso%5B%5D=${resource}`
        : '/chamadaspublicas'

    const { data } = await axios.get(proxy + baseUrl + path)
    const $ = load(data)
    const publicCalls = $('#conteudoChamada .item')

    const arrayPublicCalls: Array<PublicCallProps> = []

    publicCalls.each((index, publicCall) => {
      const id = parseInt(
        `${$(publicCall).children('h3').children('a').attr('href')}`.split(
          '/'
        )[3]
      )
      const title = $(publicCall).children('h3').text()
      const deadline = $(publicCall)
        .children('div.prazo')
        .children('span')
        .text()
      const publication = $(publicCall)
        .children('div.data_pub')
        .children('span')
        .text()
      const source = $(publicCall)
        .children('div.fonte')
        .children('span')
        .text()
        .trim()
      const sourceURI = `http://localhost:3000/chamadaspublicas/resource/${source}`
      const details = `http://localhost:3000/chamadaspublicas/${id}`

      arrayPublicCalls.push({
        id,
        attributes: {
          title,
          schedule: {
            deadline,
            publication,
          },
          source: {
            uri: sourceURI,
          },
        },
        links: {
          self: details,
        },
      })
    })

    res.json({ arrayPublicCalls })
  }

  async findUnique(req: Request, res: Response) {
    const { id } = req.params
    const proxy = 'https://api.codetabs.com/v1/proxy?quest='
    const baseUrl = `http://www.finep.gov.br/chamadas-publicas`
    const path = `/chamadapublica/${id}`

    const { data } = await axios.get(proxy + baseUrl + path)
    const $ = load(data)

    const publicCallDetails: PublicCallDetailsProps = {
      id: parseInt(id),
      attributes: {
        title: $('.item_fields').children('.tit_pag').text(),
        description: $('.item_fields')
          .children('.desc')
          .children('.text')
          .text(),
        schedule: {
          publication: $('.item_fields')
            .children('.group:nth-child(3)')
            .children('.text')
            .text(),
          deadline: $('.item_fields')
            .children('.group:nth-child(4)')
            .children('.text')
            .text(),
        },
        source: {
          uri: `${proxy + baseUrl}/chamadaspublicas?frecurso%5B%5D=${$(
            '.item_fields'
          )
            .children('.group:nth-child(5)')
            .children('.text')
            .text()
            .trim()}`,
        },
      },
    }

    res.json({ publicCallDetails })
  }
}
