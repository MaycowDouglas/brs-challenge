import express from 'express'
import PublicCallsRoutes from './modules/PublicCalls/routes'

const server = express()

server.use(express.json())
server.use('/chamadaspublicas', PublicCallsRoutes)

server.listen(3000, () => console.log('listening on port 3000'))
